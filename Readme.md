CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module add some enhancements for layout builder.

*View Block*

A view block with automatic offset calculation.

*Layout builder preview*

Add a new view mode for blocks with a complex layout to show a preview in layout builder.

REQUIREMENTS
------------

This module requires the following modules:

 * Drupal core layout_builder

RECOMMENDED MODULES
-------------------

 * Layout Builder Restrictions (https://www.drupal.org/project/layout_builder_restrictions):

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

- Configure preview display for blocks.

TROUBLESHOOTING
---------------

FAQ
---

MAINTAINERS
-----------

Current maintainers:
 * Erik Seifert - https://www.drupal.org/u/erik-seifert

 This project has been sponsored by:
 * b-connect GmbH
   Visit https://www.b-connect.de for more information.
