<?php

namespace Drupal\Tests\layout_builder_enhancements\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Functional tests for the layout_builder_enhancements views feature.
 *
 * @group layout_builder_enhancements
 */
class LayoutBuilderEnhancementsViewsTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    // Currently no proper schema configured. Be less strict for the time
    // being and fix it at a later stage.
    //
    // Tightly connected to
    // https://www.drupal.org/project/drupal/issues/3015152
    'core.entity_view_display.node.landingpage.default',
  ];

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'layout_builder_enhancements_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The list of created nodes during testing.
   *
   * @var \Drupal\node\NodeInterface[]
   */
  protected $nodes;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->nodes[] = $this->createNode([
      'type' => 'landingpage',
      'title' => 'Frontpage',
    ]);

    $this->nodes[] = $this->createNode([
      'type' => 'article',
      'title' => 'Article 1',
    ]);
    $this->nodes[] = $this->createNode([
      'type' => 'article',
      'title' => 'Article 2',
    ]);
    $this->nodes[] = $this->createNode([
      'type' => 'article',
      'title' => 'Article 3',
    ]);
    $this->nodes[] = $this->createNode([
      'type' => 'article',
      'title' => 'Article 4',
    ]);
    $this->nodes[] = $this->createNode([
      'type' => 'article',
      'title' => 'Article 5',
    ]);
  }

  /**
   * Make sure the LBE Views blocks are respecting the pager settings.
   *
   * @return void
   */
  public function testAddViewsBlock() {
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('admin/structure/types/manage/landingpage/display/default/layout');
    $this->clickLink('Add section');
    $this->clickLink('Three column');
    $this->getSession()->getPage()->pressButton('Add section');

    // Create first block.
    $this->clickLink('Add block in Section 1, First region');
    $this->clickLink('Articles: Inline');
    $this->getSession()->getPage()->checkField('Should respect pager');
    $this->getSession()->getPage()->selectFieldOption('View mode', 'teaser');
    $this->getSession()->getPage()->pressButton('Add block');

    // Create second block.
    $this->clickLink('Add block in Section 1, Second region');
    $this->clickLink('Articles: Inline');
    $this->getSession()->getPage()->checkField('Should respect pager');
    $this->getSession()->getPage()->selectFieldOption('View mode', 'teaser');
    $this->getSession()->getPage()->pressButton('Add block');

    // Create second block.
    $this->clickLink('Add block in Section 1, Third region');
    $this->clickLink('Articles: Inline');
    $this->getSession()->getPage()->checkField('Should respect pager');
    $this->getSession()->getPage()->selectFieldOption('View mode', 'teaser');
    $this->getSession()->getPage()->pressButton('Add block');

    // Save the layout.
    $this->getSession()->getPage()->pressButton('Save layout');

    // Visit the landingpage and check the layout.
    $this->drupalGet('node/1');
    $this->assertSession()->linkExists('Article 1');
    $this->assertSession()->linkExists('Article 2');
    $this->assertSession()->linkExists('Article 3');
    $this->assertSession()->linkNotExists('Article 4');
    $this->assertSession()->linkNotExists('Article 5');

    // Configure the a new section + block to show all remaining items.
    $this->drupalGet('admin/structure/types/manage/landingpage/display/default/layout');
    $this->clickLink('Add section at end of layout');
    $this->clickLink('One column');
    $this->getSession()->getPage()->pressButton('Add section');
    $this->clickLink('Add block in Section 2, Content region');
    $this->clickLink('Articles: Inline');
    $this->getSession()->getPage()->checkField('Should respect pager');
    $this->getSession()->getPage()->checkField('Show all remaining rows');
    $this->getSession()->getPage()->selectFieldOption('View mode', 'teaser');
    $this->getSession()->getPage()->pressButton('Add block');
    $this->getSession()->getPage()->pressButton('Save');

    // Visit the landingpage and check that article 4 and 5 are shown, so all
    // remaining items.
    $this->drupalGet('node/1');
    $this->assertSession()->linkExists('Article 1');
    $this->assertSession()->linkExists('Article 2');
    $this->assertSession()->linkExists('Article 3');
    $this->assertSession()->linkExists('Article 4');
    $this->assertSession()->linkExists('Article 5');

  }

}
