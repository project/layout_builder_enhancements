<?php

namespace Drupal\layout_builder_enhancements_blocks\Controller;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder\Controller\ChooseBlockController as BaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to choose a new block.
 *
 * @internal
 *   Controller classes are internal.
 */
class ChooseBlockController extends BaseController {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ChooseBlockController constructor.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(BlockManagerInterface $block_manager, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, ModuleHandlerInterface $module_handler) {
    parent::__construct($block_manager, $entity_type_manager, $current_user);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * Provides the UI for choosing a new block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  #[\Override]
  public function build(SectionStorageInterface $section_storage, int $delta, $region) {
    $build = parent::build($section_storage, $delta, $region);
    $inline_blocks = $this->inlineBlockList($section_storage, $delta, $region);
    unset($inline_blocks['back_button']);
    $build['block_categories'][] = [
      '#type' => 'details',
      '#attributes' => ['class' => ['js-layout-builder-category']],
      '#weight' => -100,
      '#open' => TRUE,
      '#title' => $this->t('Content'),
      'links' => $inline_blocks,
    ];
    unset($build['add_block']);
    unset($build['filter']);
    return $build;
  }

  /**
   * Get inline block categories.
   *
   * @return array
   *   Categories as index and types as values.
   */
  protected function getInlineBlockCategories(): array {
    $categories = [];
    /**
     * @var \Drupal\block_content\Entity\BlockContentType[]
     */
    $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple();
    foreach ($types as $type) {
      $cat = $type->getThirdPartySetting('layout_builder_enhancements_blocks', 'category', NULL);
      if ($cat) {
        if (!isset($categories[$cat])) {
          $categories[$cat] = [];
        }
        $categories[$cat][] = $type->id();
      }
    }
    return $categories;
  }

  /**
   * Provides the UI for choosing a new inline block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  #[\Override]
  public function inlineBlockList(SectionStorageInterface $section_storage, int $delta, $region) {
    $build = parent::inlineBlockList($section_storage, $delta, $region);
    $restriction_plugins = [];

    if ($this->moduleHandler->moduleExists('layout_builder_restrictions')) {
      // @phpstan-ignore-next-line
      $layout_builder_restrictions_manager = \Drupal::service('plugin.manager.layout_builder_restriction');
      $restriction_plugins = $layout_builder_restrictions_manager->getSortedPlugins();
    }
    foreach (array_keys($restriction_plugins) as $id) {
      $plugin = $layout_builder_restrictions_manager->createInstance($id);
      $allowed_inline_blocks = $plugin->inlineBlocksAllowedinContext($section_storage, $delta, $region);

      // Loop through links and remove those for disallowed inline block types.
      foreach ($build['links']['#links'] as $key => $link) {
        $route_parameters = $link['url']->getRouteParameters();
        if (!in_array($route_parameters['plugin_id'], $allowed_inline_blocks)) {
          unset($build['links']['#links'][$key]);
        }
      }
    }
    foreach ($build['links']['#links'] as &$link) {
      /**
       * @var \Drupal\Core\Url
       */
      $url = $link['url'];
      $pluginId = explode(':', $url->getRouteParameters()['plugin_id']);
      /**
       * @var \Drupal\block_content\Entity\BlockContentType
       */
      $type = $this->entityTypeManager->getStorage('block_content_type')->load($pluginId[1]);
      $icon = $type->getThirdPartySetting('layout_builder_enhancements_blocks', 'icon');
      $weight = $type->getThirdPartySetting('layout_builder_enhancements_blocks', 'weight', 100);
      $link['weight'] = $weight;
      if ($icon) {
        $link['attributes']['class'][] = 'component-wrapper';
        $link['title'] = [
          '#type' => 'inline_template',
          '#template' => '<span class="img">{{ icon|raw }}</span> <span class="title">{{ title }}</span>',
          '#attached' => [
            'library' => ['layout_builder_enhancements_blocks/inline-block-icon'],
          ],
          "#context" => [
            'title' => $link['title'],
            'icon' => $icon,
          ],
        ];
      }
    }

    uasort($build['links']['#links'], function ($a, $b) {
      if ($a['weight'] > $b['weight']) {
        return 1;
      }
      if ($a['weight'] < $b['weight']) {
        return -1;
      }
      return 0;
    });
    return $build;
  }

}
