(function ($, Drupal) {

  let observer = new IntersectionObserver(function (entries, observer) {
    $('#drupal-off-canvas .section.active').removeClass
    entries.filter(function (e) {
      return e.isIntersecting;
    }).filter(v => v.intersectionRatio).forEach(function (entry) {
      let e = $('#drupal-off-canvas #section-' + $(entry.target).data('type').delta);
      if (entry.intersectionRatio > 0.2) {
        e.addClass('active');
      } else {
        e.removeClass('active');
      }
    })
    // scrollToActive();
  }, {
    threshold: [0.0, 0.2, 1.0]
  });

  let scrollToActive = function() {
    if ($('#drupal-off-canvas').find('li.section.active').length == 0) {
      return;
    }
    let first = $('#drupal-off-canvas').find('li.section.active').eq(0);
    let scroll = $('#drupal-off-canvas').find('li.section.active').eq(0).position().top - $('#drupal-off-canvas').position().top;

    if (scroll > $('#drupal-off-canvas').scrollTop()) {
      $('#drupal-off-canvas').scrollTop(scroll);
      return;
    }
  }

  let scrollTo = function(type, el) {
    $('body, html').animate({ scrollTop: $(type.selector).offset().top })
  }

  let highlight = function (type, el) {
    $(type.selector).addClass('layout-builder-visual-hover');
  }

  let dehighlight = function (type, el) {
    $(type.selector).removeClass('layout-builder-visual-hover');
  }

  let render = function(childs) {
    let content = $("<ul></ul>");
    for (let i = 0; i < childs.length; i++) {
      let cur = childs[i];
      let e = $(`<li id="${cur.type}-${cur.delta}" class="${cur.type} ${cur.type}-${cur.delta}"><span class="${cur.type} group-label">${cur.label}</span></li>`);
      e.find('span').eq(0).data('type', cur).on('click', function() {
        $('#drupal-off-canvas').find('li.section.active').removeClass('active');
        scrollTo($(this).data('type'), $(this));
      }).on('mouseenter', function () {
        highlight($(this).data('type'), $(this));
      }).on('mouseleave', function () {
        dehighlight($(this).data('type'), $(this));
      });
      if (cur.childs && cur.childs.length > 0) {
        e.find('span').after(render(cur.childs));
      }
      content.append(e);
    }
    return content;
  }

  //var CKEDITOR = CKEDITOR;
   Drupal.AjaxCommands.prototype.ShowInfoCommand = function (ajax, response, status ) {
     let sections = [];
     $('#layout-builder').find('.layout-builder__section').each(function() {
       let layout = $(this).find('.layout-builder__layout').eq(0);
       let section = {
         type: 'section',
         delta: layout.attr('data-layout-delta'),
         selector: '[data-layout-delta=' + layout.attr('data-layout-delta') + ']',
         label: $(this).attr('aria-label'),
         childs: [],
       }
       observer.observe(this);
       $(this).find('.layout-builder__region').each(function() {
         let region = {
           type: 'region',
           label: $(this).attr('aria-label'),
           region: $(this).attr('data-region'),
           delta: section.delta + '-' + section.childs.length,
           selector: section.selector + ' [data-region=' + $(this).attr('data-region') + ']',
           childs: [],
         }
         $(this).find('.layout-builder-block').each(function() {
           let block = {
             type: 'block',
             delta: region.delta + '-' + region.childs.length,
             label: $(this).attr('data-layout-content-preview-placeholder-label'),
             selector: '[data-layout-block-uuid=' + $(this).attr('data-layout-block-uuid') + ']',
             uuid: $(this).attr('data-layout-block-uuid'),
           }
           region.childs.push(block);
         })
         section.childs.push(region);
       })
       $(this).data('type', section);
       sections.push(section);
     });
     $('#layout-builder-visual-tree').append(render(sections));
   }

})(jQuery, Drupal);

