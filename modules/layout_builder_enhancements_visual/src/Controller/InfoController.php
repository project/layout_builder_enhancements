<?php

namespace Drupal\layout_builder_enhancements_visual\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\OpenOffCanvasDialogCommand;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Ajax\CommandInterface;

/**
 * Class ExtendCommand.
 */
class ShowInfoCommand implements CommandInterface {

  #[\Override]
  public function render() {
    return [
      'command' => 'ShowInfoCommand',
    ];
  }

}


/**
 * Returns responses for Layout Builder Enhancements routes.
 */
class InfoController extends ControllerBase {
  use StringTranslationTrait;

  /**
   * Builds the response.
   */
  public function build() {
    $build['content'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'layout-builder-visual-tree'],
      '#attached' => ['library' => ['layout_builder_enhancements_visual/info']],
    ];

    $response = new AjaxResponse();
    $response->addCommand(new OpenOffCanvasDialogCommand(
      $this->t('Structure'),
      $build,
      ['dialogClass' => 'layout-builder-visual-tree-widget']
    ));

    $response->addCommand(new ShowInfoCommand());
    return $response;
  }

}
