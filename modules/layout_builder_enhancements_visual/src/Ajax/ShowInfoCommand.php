<?php

namespace Drupal\layout_builder_enhancements_visual\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class ExtendCommand.
 */
class ShowInfoCommand implements CommandInterface {

  #[\Override]
  public function render() {
    return [
      'command' => 'ShowInfoCommand',
    ];
  }

}
