<?php

namespace Drupal\layout_builder_enhancements\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;

/**
 * EventSubscriber for layout render event.
 */
class EventSubscriber implements EventSubscriberInterface {

  protected $displayRepository;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_repository_display
   *   For getting Facebook and SimpleFbConnectPersistentDataHandler services.
   */
  public function __construct(EntityDisplayRepositoryInterface $entity_repository_display) {
    $this->displayRepository = $entity_repository_display;
  }


  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function getSubscribedEvents(): array {
    return [
      LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY => [
        'onBuildRender',
        -100,
      ],
    ];
  }

  /**
   * Event render function.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $block = $block = $event->getPlugin();
    if (!$block instanceof InlineBlock) {
      return;
    }

    if (!$event->inPreview()) {
      return;
    }
    $displays = $this->displayRepository->getViewModeOptionsByBundle('block_content', $block->getDerivativeId());
    if (!isset($displays['layout_builder_preview'])) {
      return;
    }

    $build = $event->getBuild();
    $block->setConfiguration(['view_mode' => 'layout_builder_preview']);
    $build['content'] = $block->build();
    $event->setBuild($build);
  }

}
